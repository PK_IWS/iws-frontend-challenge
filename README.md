# IWS Frontend Assessment
## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```
## Overview
The purpose of this app is to read a raw value, supplied by a 4-20 mA sensor publishing over an mqtt network, and then scale it into a useful output value.

For this context, lets assume the input is coming from a thermometer and thus the output value is a temperature.

*Estimated completion time: 2-3 hours*

## AI Tool Usage
While we have no problem with the usage of AI tools (e.g. GitHub CoPilot) in everyday development, this exercise is intended to test your own knowledge and technical abilities. We therefore encourage you to temporarily disable or refrain from using any AI tools while writing code for this project. We will ask you questions about the code you have written and it is relatively easy to spot where AI autocompletion has been used.

---
## Task#1:
Understand MQTT and how it can be used in the browser. Add an unsubscribe function.

You can learn more about MQTT here: https://www.hivemq.com/mqtt-essentials/

For this app you will only need to understand how to publish and subscribe to messages.

For this app we are connecting to a publicly hosted mqtt broker: test.mosquitto.org so be aware subscribing to all topics ('#') is not recommended.
If you have issues connecting to the broker, try using:
- a different browser (e.g. firefox)
- a secure websocket port, e.g. 8081 ([you may need to enable the useSSL option](https://eclipse.dev/paho/files/jsdoc/Paho.MQTT.Client.html#:~:text=useSSL))
- a different public broker, such as hivemq
- a different client id
- deploy your own broker locally using docker, and launch the full app with docker compose. Here are some links to get you started:
   - Mosquitto container: https://hub.docker.com/_/eclipse-mosquitto)
   - Vue container: https://v2.vuejs.org/v2/cookbook/dockerize-vuejs-app.html?redirect=true
   - Note: This option will add significant time however it shows us you have good understanding of docker, so bonus points if you do it, but it is not a requirement or priority.

### Requirements
- See the `mqtt.js` file to understand the MQTT wrapper class and underlying Paho class.
- Add meaningful comments to the file to help the next dev understand what you've learnt.
- Create an unsubscribe function.
---
## Task #2:
Create a Vue component which publishes a value over an MQTT network. This will simulate the 4-20mA sensor with an expected range of values.
### Requirements
- For this task, the `RawPublisher.vue` file should be modified.
- The value should only publish once per second.
- The value should oscillate continuously between 0 and 20, at a configurable rate of change (default to 0.5mA/second).
   - Note that oscillate is defined as moving steadily (not randomly) back and forth between two points.
   - The rate of change interval should remain CONSTANT at 1 second.
   - Example with output printed each second:
   ```
   UTC Time                Output
   ---------------------|--------
   2025-03-11T20:16:25Z    0
   2025-03-11T20:16:26Z    0.5
   2025-03-11T20:16:27Z    1
   2025-03-11T20:16:28Z    1.5
   2025-03-11T20:16:29Z    2
   2025-03-11T20:16:30Z    2.5
   2025-03-11T20:16:31Z    3
   2025-03-11T20:16:32Z    3.5
   ...
   2025-03-11T20:17:06Z    19
   2025-03-11T20:17:07Z    19.5
   2025-03-11T20:17:08Z    20
   2025-03-11T20:17:09Z    19.5
   2025-03-11T20:17:10Z    19
   ...
   2025-03-11T20:17:47Z    1.5
   2025-03-11T20:17:48Z    1
   2025-03-11T20:17:49Z    0.5
   2025-03-11T20:17:50Z    0
   2025-03-11T20:17:51Z    0.5
   2025-03-11T20:17:52Z    1
   ```
- The publish topic should be: `iws_<your first name>`
   - Example: `iws_patrick`
- The payload should be a JSON of format: `{"value": <number> }`
   - Example: `{"value": 5}`
---
## Task #3:
Create a Vue component which reads a raw sensor value from the network and scales it to the desired output value, and indicates the value with color-based warnings.

You can read more about 4-20 mA sensors and how to scale to an output value here: https://www.divize.com/techinfo/4-20ma-calculator.html
### Requirements
- Display two float-value indicator boxes labelled:
   - Input
   - Output
- The input value should be received via mqtt (published by the RawPublisher component)
- The output value should display the scaled value.
- If the input value is less than 4, the input indicator text color should be red, and the output value should read "N/A"
- If the output value is in the top 25% of the output value range, the output indicator box background colour should be red.
- if the output value is in the bottom 25% of the output value range, the output indicactor box background colour should be blue.
- A form should allow the following values to be adjusted:
	- Raw Low (default: 4mA)
	- Raw High (default: 20 mA)
	- Engineering Low (default: -70 deg C)
	- Engineering High (default: 70 deg C)
  - Oscillation speed  of raw publisher (default: 0.5 mA/s)

## Submission steps
Once you have completed ALL 3 tasks, you can submit your code:
- Please create a .ZIP file of your project (without the npm dependencies) and upload to any cloud storage that can be shared with us.
- Not required, but helpful if a .git file is included in the zip and shows history of change from the original.
- DO NOT push your work back to this repository.